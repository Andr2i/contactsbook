package com.andr2i.contactsbook.ui.fragment;

import android.os.Bundle;

import android.view.View;

import androidx.annotation.Nullable;

import com.andr2i.contactsbook.R;
import com.andr2i.contactsbook.common.fragments.BaseFragment;

public class SettingsFragment extends BaseFragment{

    public SettingsFragment() {}

    public static SettingsFragment newInstance() {
        return new SettingsFragment();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextPlaceholder.setText(String.format("%s fragment", getString(R.string.nav_menu_title_settings)));
    }


}

package com.andr2i.contactsbook.common;

import android.graphics.Rect;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

public class CustomItemDecoration extends RecyclerView.ItemDecoration{

    private int mVertical;
    private int mHorizontal;

    public CustomItemDecoration(int vertical, int horizontal) {
        mVertical = vertical;
        mHorizontal = horizontal;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.left = mHorizontal;
        outRect.right = mHorizontal;
        outRect.bottom = mVertical;
        outRect.top = mVertical;
    }

}


package com.andr2i.contactsbook.common;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.andr2i.contactsbook.ui.activity.MainActivity;


/**
 * References:
 * [1] https://www.bignerdranch.com/blog/splash-screens-the-right-way/
 */
public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivity.launch(SplashActivity.this);
        finish();
    }


}

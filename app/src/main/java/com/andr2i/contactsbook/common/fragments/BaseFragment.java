package com.andr2i.contactsbook.common.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.andr2i.contactsbook.R;

public class BaseFragment extends Fragment {

    protected TextView mTextPlaceholder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_placeholder, container, false);
        mTextPlaceholder = (TextView) view.findViewById(R.id.text_place_holder);

        return view;
    }


}
